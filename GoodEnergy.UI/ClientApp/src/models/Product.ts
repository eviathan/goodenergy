import SpecialOffer from './SpecialOffer';

export default interface Product {
    id: string,
    name: string,
    description: string,
    price: number, 
    image: string,
    specialOffers: Array<SpecialOffer>
}