import Product from './Product';

export interface OrderItem {
    product: Product,
    amount: number, 
    discount: number
}

export default interface Order {
    id: string,
    items: Array<OrderItem>,
    grossPrice: number,
    netPrice: number
}