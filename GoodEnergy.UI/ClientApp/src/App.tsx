import * as React from 'react';
import { Route } from 'react-router';
import Layout from './components/Layout';
import MarketPage from './containers/MarketPage'
import CheckoutPage from './containers/CheckoutPage'

import './custom.scss'

export default () => (
    <Layout>
        <Route exact path='/' component={MarketPage} />
        <Route exact path='/checkout' component={CheckoutPage} />
    </Layout>
);
