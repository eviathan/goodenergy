import * as React from 'react';
import { connect } from 'react-redux';
import { ApplicationState } from '../../store';
import { ShoppingCartState } from '../../store/shoppingCart/shoppingCart.types';
import { OrderState } from '../../store/order/order.types';
import { shoppingCartActionCreators } from '../../store/shoppingCart/shoppingCart.actions';
import { orderActionCreators } from '../../store/order/order.actions';
import Blurb from '../../components/Blurb';
import Reciept from '../../components/Reciept';

import './CheckoutPage.scss';

type Props = 
  OrderState
  & ShoppingCartState
  & typeof orderActionCreators

class CheckoutPage extends React.PureComponent<Props> {

  public componentDidMount() {    
    this.props.requestCreateOrder({
      items: Object.entries(this.props.items).map(([productId, item]) => {
        return {
         productId,
         amount: item.amount
        }
      })
    })
  }

  public render() {

    let title = "Checkout";
    let subtitle = "Almost done! Please double check everything is in order";

    const order = this.props.order;

    if(!order || order.items.length <= 0)
    {
      title = "No order found";
      subtitle = "Please return to the marketplace to place your order";
    }

    return (
      <div className="checkout-page">
        <Blurb title={title} subtitle={subtitle} />
        { order && order.items.length > 0 && !this.props.isLoading
          ? <Reciept {...order} />
          : null }
      </div>
    );
  }
}

export default connect(
  (state: ApplicationState) => {
    return {
      ...state.shoppingCart,
      ...state.order
    }
  },
  {
    ...shoppingCartActionCreators,
    ...orderActionCreators
  }
)(CheckoutPage as any); // eslint-disable-line @typescript-eslint/no-explicit-any
