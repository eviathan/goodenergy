import * as React from 'react';
import { connect } from 'react-redux';
import { ApplicationState } from '../../store';
import { ProductState } from '../../store/product/product.types';
import { ShoppingCartState } from '../../store/shoppingCart/shoppingCart.types';
import { productActionCreators } from '../../store/product/product.actions';
import ProductCard from '../../components/ProductCard';
import { shoppingCartActionCreators } from '../../store/shoppingCart/shoppingCart.actions';
import Product from '../../models/Product';
import Blurb from '../../components/Blurb';

import './MarketPage.scss';

type Props = 
  ProductState
  & ShoppingCartState
  & typeof productActionCreators
  & typeof shoppingCartActionCreators

class MarketPage extends React.PureComponent<Props> {

  public componentDidMount() {    
    this.props.requestAllProducts();
  }

  public onAddProductToCart(product: Product, amount: number) {
    this.props.addItemToShoppingCart(product, amount);
  }

  componentWillReceiveProps(nextProps: any) {
    this.setState({
      ...this.state,
      shoppingCart: {
        items: nextProps.items
      }
    })
  }
  
  public render() {
    return (
      <div className="market-page">
        <Blurb 
          title="Marketplace"
          subtitle=" Choosing to eat clean is one of the best things you can do to tackle your hunger crisis. Here’s how we can help" />
        <ul className="product-list">
            { this.props.products.map((product, i) => 
            {
              return (<li key={i}>
                <ProductCard
                  amount={ this.props.items[product.id] ? this.props.items[product.id].amount : 0 }
                  product={product} 
                  didChangeAmount={(product, amount) => this.onAddProductToCart(product, amount)} />
              </li>);
            }
            )}
        </ul>
      </div>
    );
  }
}

export default connect(
  (state: ApplicationState) => {
    return {
      ...state.products,
      ...state.shoppingCart
    }
  },
  {
    ...productActionCreators,
    ...shoppingCartActionCreators
  }
)(MarketPage as any); // eslint-disable-line @typescript-eslint/no-explicit-any
