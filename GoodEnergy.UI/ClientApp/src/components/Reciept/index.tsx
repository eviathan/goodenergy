import * as React from 'react';
import Tags from '../../components/Tags';
import Order from '../../models/Order';

import './Reciept.scss';

export default function Reciept({ netPrice, grossPrice, items }: Order) {
    return (
        <div className="reciept">
            <div className="order">
                <ul className="items">
                    { items.map((item, i) => {
                        const specialOffers = item.product.specialOffers.map(offer => offer.detail);
                        return (
                            <li key={i}>
                                <div className="item">
                                    <img src={item.product.image} alt={item.product.name} />
                                    <div className="details">
                                        <h3>{`${item.amount}✕ ${item.product.name}`}</h3>
                                        <p className="description">{item.product.description}</p>
                                        <div className="prices">
                                            <p><span>Price: </span>£{(item.product.price * item.amount).toFixed(2)}</p>
                                            { item.discount > 0 ? <p><span>Discount: </span>£{item.discount.toFixed(2)}</p> : null }
                                        </div>
                                    </div>
                                </div>
                                { 
                                    // NOTE: This works as there are no offers with multiple special offers
                                    // A more robust sollution will be needed if that changes
                                    item.discount > 0 
                                        ? <Tags tags={specialOffers} /> 
                                        : null 
                                } 
                            </li>
                        );
                    })}
                </ul>
            </div>
            <h1>Subtotal: £{grossPrice.toFixed(2)}</h1>
            <h1>Total: £{netPrice.toFixed(2)}</h1>
        </div>
    );
}
