import * as React from 'react';
import { connect } from 'react-redux';
import { ApplicationState } from '../../store';
import { ProductState } from '../../store/product/product.types';
import { ShoppingCartState } from '../../store/shoppingCart/shoppingCart.types';
import { productActionCreators } from '../../store/product/product.actions';
import { shoppingCartActionCreators } from '../../store/shoppingCart/shoppingCart.actions';

import './ShoppingCart.scss';

type Props = 
  ProductState
  & ShoppingCartState
  & typeof productActionCreators
  & typeof shoppingCartActionCreators

class ShoppingCart extends React.PureComponent<Props> {

    componentWillReceiveProps(nextProps: any) {
        this.setState({
            ...this.state,
            shoppingCart: {
            items: nextProps.items
            }
        })
    }

    render() {

        const { items } = this.props

        console.log(this.props)
        return (
            <div className="shopping-cart">  
                <button className="cart">WOOTER</button>
                <div className="speech-bubble">
                    <h3>Basket</h3>
                    <ul>
                        {Object.entries(items).map(([_, item]) => (
                            <li>
                                <div>{item.product.name}</div>
                                <div>{item.amount}</div>
                            </li>
                        ))}
                    </ul>
                    <button className="checkout">Checkout</button>
                </div>
            </div>
        );
    }
}

export default connect(
    (state: ApplicationState) => {
      return {
        ...state.products,
        ...state.shoppingCart
      }
    },
    {
      ...productActionCreators,
      ...shoppingCartActionCreators
    }
  )(ShoppingCart as any); // eslint-disable-line @typescript-eslint/no-explicit-any
  