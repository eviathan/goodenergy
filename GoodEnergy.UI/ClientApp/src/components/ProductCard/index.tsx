import * as React from 'react';
import Product from '../../models/Product';
import Tags from '../Tags';
import Counter from '../Counter';
import ShopButton from '../ShopButton';

import './ProductCard.scss';

interface ProductCardProps {
    product: Product,
    amount?: number | undefined
    didChangeAmount: (product: Product, amount: number) => void,
}

export default function ProductCard({ product , amount, didChangeAmount }: ProductCardProps) {

    const hasProducts = !!amount && amount > 0;
    
    return (
        <div className="product-card">            
            <Counter amount={amount} >
                <img src={product.image} alt={product.name} />
            </Counter>
            <Tags tags={product.specialOffers.map(offer => offer.detail)} />
            <h1>{product.name}</h1>
            <h4>£{product.price.toFixed(2)}</h4>
            <h3>{product.description}</h3>
            <ShopButton 
                showCounter={hasProducts}
                didChangeAmount={(amount) => didChangeAmount(product, amount)}
            />
        </div>
    );
}
