import * as React from 'react';
import { FunctionComponent } from 'react';

import './Counter.scss';

interface CounterButtonProps {
    amount: number | undefined
}

const CounterButton: FunctionComponent<CounterButtonProps> = (props) => {

    const { amount, children } = props;

    return (
        <div className="counter-wrapper">
            { children }
            { amount && amount > 0 ?
                <div className="counter">{amount}</div> : null 
            }
        </div>
    );
}

export default CounterButton;