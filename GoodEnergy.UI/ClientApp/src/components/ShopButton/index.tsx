import * as React from 'react';

import './ShopButton.scss';

interface ShopButtonProps {
    showCounter?: boolean | undefined
    didChangeAmount: (amount: number) => void,
}

export default function ShopButton({ showCounter, didChangeAmount }: ShopButtonProps) {
    
    return (
        <div className="shop-button">            
            { showCounter 
                ? (
                    <>
                        <button className="subtract" onClick={() => didChangeAmount(-1)}>-</button>
                        <button className="add" onClick={() => didChangeAmount(1)}>+</button>
                    </>
                )
                :  <button onClick={() => didChangeAmount(1)}>Add to cart</button>
            }
        </div>
    );
}
