import * as React from 'react';
import { useHistory, useLocation } from 'react-router';
import { useSelector } from 'react-redux';

import './Navigation.scss';
import { ApplicationState } from '../../store';

export default function Navigation() {

    const history = useHistory();
    const location = useLocation();
    const shoppingCart = useSelector((state: ApplicationState) => state.shoppingCart);
    const shoppingCartHasItems = shoppingCart 
        && shoppingCart.items 
        && Object.entries(shoppingCart.items)
                 .map(([_, item]) => item.amount)
                 .reduce((a, b) => a + b, 0) > 0;
        
    return (
        <header>
            <div className="wrapper">
                <div className="logo-wrapper">
                    <img className="logo" src="https://www.goodenergy.co.uk/media/18273/bio-fuel-leaf-yellow-circle.png" alt="logo" />
                    <h1>good foodery</h1>
                </div>
                { shoppingCartHasItems 
                    ? location.pathname === '/checkout'
                        ? <button onClick={() => history.push('/')}>Back<i></i></button>
                        : <button onClick={() => history.push('/checkout')}>Checkout<i></i></button>
                    : null
                }
            </div>
        </header>
    );
}