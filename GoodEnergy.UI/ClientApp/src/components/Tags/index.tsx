import * as React from 'react';
import './Tags.scss';

function Tag({ label }: { label: string}) {
    return (
        <li><a href="/" onClick={(e) => e.preventDefault()}>{label}</a></li>
    );
}

export default function Tags({ tags }: { tags: Array<string>}) {
    
    return (
        <ul className="tags">
            {tags.map((tag, i) => <Tag key={i} label={tag} />)}
        </ul>
    );
}
