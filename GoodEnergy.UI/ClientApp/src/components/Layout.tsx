import * as React from 'react';
import Navigation from './Navigation';

export default class Layout extends React.PureComponent<{}, { children?: React.ReactNode }> {
    public render() {
        return (
            <React.Fragment>
                <Navigation />
                <main>
                    {this.props.children}
                </main>
            </React.Fragment>
        );
    }
}