import * as React from 'react';

import './Blurb.scss';

interface BlurbProps {
    title: string,
    subtitle: string
}

export default function Blurb({ title, subtitle }: BlurbProps) {
    
    return (
        <div className="blurb">
            <h2>{title}</h2>
            <h3>{subtitle}</h3>
      </div>
    );
}
