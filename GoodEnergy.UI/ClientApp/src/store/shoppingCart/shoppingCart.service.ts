import Product from '../../models/Product';
import { ApplicationState } from '../../store';
import { ShoppingCartItem } from './shoppingCart.types';

export default class ShoppingCartService {

    private _state: ApplicationState

    constructor(state: ApplicationState) {
        this._state = state;        
    }

    public getShoppingCartItems(product: Product, amount: number): { 
        [key:string]: ShoppingCartItem
    }{

        const shoppingCart = this._state.shoppingCart;
        let items: { [key:string]: ShoppingCartItem } = {};

        if(shoppingCart) {
            items = shoppingCart.items;
            
            if(items){
                const item = items[product.id];    
                if(item) {
                    item.amount += amount;
                } else {
                    items[product.id] = {
                        product: product,
                        amount: amount
                    }
                }
            }
        }

        return items;
    }
}