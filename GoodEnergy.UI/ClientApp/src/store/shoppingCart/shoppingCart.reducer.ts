﻿import { Action, Reducer } from 'redux';
import { ShoppingCartState } from './shoppingCart.types';
import { 
    KnownAction,
} from './shoppingCart.actions';

const unloadedState:ShoppingCartState = {
    isLoading: false,
    items: {}
}

export const reducer: Reducer<ShoppingCartState> = 
(state: ShoppingCartState | undefined, incomingAction: Action): ShoppingCartState => {

    if (state === undefined) 
        return unloadedState;

    const action = incomingAction as KnownAction;

    switch (action.type) {
        case "REQUEST_ADD_ITEM_TO_CART":
            return {
                items: state.items,
                isLoading: true
            };
        case "RECIEVE_SHOPPING_CART_ITEMS":
            return {
                items: action.items,
                isLoading: false
            };
    }

    return state;
};
