import { AppThunkAction } from '..';
import { ShoppingCartItem } from './shoppingCart.types';
import ShoppingCartService from './shoppingCart.service';
import Product from '../../models/Product';

export type KnownAction = 
    RequestAddItemToShoppingCartAction
    | RecieveShoppingCartAction

export interface RequestAddItemToShoppingCartAction {
    type: 'REQUEST_ADD_ITEM_TO_CART';
    product: Product;
    amount: number;
}

export interface RecieveShoppingCartAction {
    type: 'RECIEVE_SHOPPING_CART_ITEMS';
    items: {
        [key: string]:ShoppingCartItem
    };
}

export const shoppingCartActionCreators = {
    addItemToShoppingCart: (product: Product, amount: number): AppThunkAction<KnownAction> => (dispatch, getState) => {

        const appState = getState();
        
        if (appState) {
            
            dispatch({ 
                type: 'REQUEST_ADD_ITEM_TO_CART',
                product: product,
                amount: amount
            });        
            
            const shoppingCartService = new ShoppingCartService(appState);
            const items = shoppingCartService.getShoppingCartItems(product, amount);

            dispatch({
                type: 'RECIEVE_SHOPPING_CART_ITEMS',
                items: items
            })
        }
    },
};