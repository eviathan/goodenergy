import Product from '../../models/Product';

export interface ShoppingCartItem {
    product: Product,
    amount: number
}

export interface ShoppingCartState {
    isLoading: boolean;
    items: {
        [key: string]: ShoppingCartItem;
    }
}