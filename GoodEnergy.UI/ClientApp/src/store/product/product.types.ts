import Product from '../../models/Product';

export interface ProductState {
    isLoading: boolean;
    products: Array<Product>;
}