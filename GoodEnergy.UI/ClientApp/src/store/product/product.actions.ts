import { AppThunkAction } from '..';
import Product from '../../models/Product';
import { Constants } from '../../constants';

export type KnownAction = RequestAllProductsAction 
    | RequestProductAction
    | RecieveAllProductsAction
    | RecieveProductAction

export interface RequestAllProductsAction {
    type: 'REQUEST_ALL_PRODUCTS';
}

export interface RequestProductAction {
    type: 'REQUEST_PRODUCT';
    id: string;
}

export interface RecieveAllProductsAction {
    type: 'RECIEVE_ALL_PRODUCTS';
    products: Array<Product>;
}

export interface RecieveProductAction {
    type: 'RECIEVE_PRODUCT';
    product: Product;
}

export const productActionCreators = {
    requestAllProducts: (): AppThunkAction<KnownAction> => (dispatch, getState) => {

        const appState = getState();

        if (appState) {
            fetch(`${Constants.API_URL}/product`)
                .then(response => {
                    return response.json() as Promise<Array<Product>>
                })
                .then(data => {
                    dispatch({ 
                        type: 'RECIEVE_ALL_PRODUCTS',
                        products: data
                    });
                });

            dispatch({ type: 'REQUEST_ALL_PRODUCTS' });
        }
    }
};