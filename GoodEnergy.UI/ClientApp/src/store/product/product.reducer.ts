﻿import { Action, Reducer } from 'redux';
import { ProductState } from './product.types';
import { 
	KnownAction,
} from './product.actions';

const unloadedState: ProductState = { 
	products: [], 
	isLoading: false 
};

export const reducer: Reducer<ProductState> = 
(state: ProductState | undefined, incomingAction: Action): ProductState => {

    if (state === undefined) 
        return unloadedState;

    const action = incomingAction as KnownAction;

    switch (action.type) {
        case "REQUEST_ALL_PRODUCTS":
            return {
                products: state.products,
                isLoading: true
            };
        case "RECIEVE_ALL_PRODUCTS":
            return {
                products: action.products,
                isLoading: false
            };
    }

    return state;
};
