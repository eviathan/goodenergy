import * as Product from './product/product.reducer';
import * as ShoppingCart from './shoppingCart/shoppingCart.reducer';
import * as Order from './order/order.reducer';
import { ProductState } from './product/product.types';
import { ShoppingCartState } from './shoppingCart/shoppingCart.types';
import { OrderState } from './order/order.types';
export interface ApplicationState {
    products: ProductState | undefined;
    shoppingCart: ShoppingCartState | undefined
    order: OrderState | undefined
}

export const reducers = {
    products: Product.reducer,
    shoppingCart: ShoppingCart.reducer,
    order: Order.reducer
};
export interface AppThunkAction<TAction> {
    (dispatch: (action: TAction) => void, getState: () => ApplicationState): void;
}
