export type ReduxAction = {
    [key: string]: {
        key: string,
        action: {
            type: string
        }
    }
}