import Order from '../../models/Order';

export interface OrderState {
    isLoading: boolean;
    order: Order | undefined;
}