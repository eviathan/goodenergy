import { AppThunkAction } from '..';
import Order from '../../models/Order';
import { Constants } from '../../constants';
import { OrderState } from './order.types';

export type KnownAction = 
    RequestCreateOrderAction 
    | RecieveCreateOrderAction

interface CreateOrderRequest {
    items: Array<{
        productId: string,
        amount: number
    }>
}

export interface RequestCreateOrderAction {
    type: 'REQUEST_CREATE_ORDER';
    orderRequest: CreateOrderRequest
}

export interface RecieveCreateOrderAction {
    type: 'RECIEVE_CREATE_ORDER';
    order: OrderState;
}

export const orderActionCreators = {
    requestCreateOrder: (orderRequest: CreateOrderRequest): AppThunkAction<KnownAction> => (dispatch, getState) => {

        const appState = getState();

        if (appState) {
            fetch(`${Constants.API_URL}/order`,
                {
                    method: 'POST',
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json'
                    },                  
                    body: JSON.stringify(orderRequest)
                })
                .then(response => {
                    return response.json() as Promise<Order>
                })
                .then(order => {
                    dispatch({ 
                        type: 'RECIEVE_CREATE_ORDER',
                        order: {
                            order: order,
                            isLoading: false
                        }
                    });
                });

            dispatch({ 
                type: 'REQUEST_CREATE_ORDER',
                orderRequest
            });
        }
    }
};