﻿import { Action, Reducer } from 'redux';
import { OrderState } from './order.types';
import { 
	KnownAction,
} from './order.actions';

const unloadedState: OrderState = { 
	order: undefined, 
	isLoading: false 
};

export const reducer: Reducer<OrderState> = 
(state: OrderState | undefined, incomingAction: Action): OrderState => {

    if (state === undefined) 
        return unloadedState;

    const action = incomingAction as KnownAction;

    switch (action.type) {
        case "REQUEST_CREATE_ORDER":
            return {
                order: state.order,
                isLoading: true
            };
        case "RECIEVE_CREATE_ORDER":
            return action.order;
    }

    return state;
};
