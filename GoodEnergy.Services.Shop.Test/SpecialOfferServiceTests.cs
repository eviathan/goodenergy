using System;
using Xunit;
using FluentAssertions;
using GoodEnergy.Core.Models;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace GoodEnergy.Services.Shop.Test
{
    public class SpecialOfferServiceTests : ShopServiceTest
    {
		[Fact]
        public async Task WhenApplyingOffersNoDiscountIsApplied()
        {
            // Arrange
            var order = new Order
            {
                Id = Guid.NewGuid(),
                CreatedDate = DateTime.Now,
                Items = new List<OrderItem<Product>>
                {
                    new OrderItem<Product>(_bread, 1)
                }
            };

            // Act
            await _shopService.ApplySpecialOffers(order);

            // Assert
            order.GrossPrice
                .Should()
                .Be(order.NetPrice);
        }

        [Fact]
        public async Task WhenApplyingOffersADisountIsApplied()
        {
            // Arrange
            var order = new Order()
            {
                Id = Guid.NewGuid(),
                CreatedDate = DateTime.Now,
                Items = new List<OrderItem<Product>>
                {
                    new OrderItem<Product>(_cheese, 2)
                },
                SpecialOffers = new List<SpecialOffer>
				{
                    _buyOneGetOneFree
				}
            };

            // Act
            await _shopService.ApplySpecialOffers(order);

            // Assert
            order.NetPrice
                 .Should()
                 .NotBe(order.GrossPrice);
        }

        [Theory]
        [InlineData(3, 6.00)]
        [InlineData(4, 6.00)]
        public async Task WhenApplyingByOneGetOneFreeOfferEveryOtherProductIsHalfPrice(int amount, decimal expectedNetPrice)
        {
            // Arrange
            var order = new Order()
            {
                Id = Guid.NewGuid(),
                CreatedDate = DateTime.Now,
                Items = new List<OrderItem<Product>>
                {
                    new OrderItem<Product>(_cheese, amount),
                },
                SpecialOffers = new List<SpecialOffer>
				{
                    _buyOneGetOneFree
				}
            };

            // Act
            await _shopService.ApplySpecialOffers(order);

            // Assert
            order.NetPrice
                 .Should()
                 .Be(expectedNetPrice);
        }

        [Theory]
        [InlineData(2, 0, 1.00)]
        [InlineData(0, 1, 2.00)]
        [InlineData(1, 1, 1.50)]
        [InlineData(1, 2, 3.50)]
        public async Task WhenApplyingByOneGetHalfPriceBreadOfferEveryOtherBreadIsHalfPrice(int productAmount, int breadAmount, decimal expectedNetPrice)
        {
            // Arrange
            var order = new Order()
            {
                Id = Guid.NewGuid(),
                CreatedDate = DateTime.Now,
                Items = new List<OrderItem<Product>>
                {
                    new OrderItem<Product>(_soup, productAmount),
                    new OrderItem<Product>(_bread, breadAmount),
                },
                SpecialOffers = new List<SpecialOffer>
				{
                    _buyOneGetOneGetHalfPriceBread
				}
            };

            // Act
            await _shopService.ApplySpecialOffers(order);

            // Assert
            order.NetPrice
                 .Should()
                 .Be(expectedNetPrice);
        }

        [Theory]
        [InlineData(1, 1.00)]
        [InlineData(2, 2.00)]
        [InlineData(3, 3.00)]
        public async Task WhenApplyingGetAThirdOffOfferProductsAreTwoThirdsThePrice(int productAmount, decimal expectedNetPrice)
        {
            // Arrange
            var order = new Order()
            {
                Id = Guid.NewGuid(),
                CreatedDate = DateTime.Now,
                Items = new List<OrderItem<Product>>
                {
                    new OrderItem<Product>(_butter, productAmount),
                },
                SpecialOffers = new List<SpecialOffer>
				{
                    _getThirdOff
				}
            };

            // Act
            await _shopService.ApplySpecialOffers(order);

            // Assert
            order.NetPrice
                 .Should()
                 .Be(expectedNetPrice);
        }

        [Theory]
        [InlineData(1, 1, 1, 1, 5.50)]
        [InlineData(1, 1, 2, 1, 6.50)]
        [InlineData(1, 2, 2, 3, 11.50)]
        public async Task WhenApplyingAMixOfOffersProductsAreTwoThirdsThePriceCorrectly(
            int soupAmount,
            int breadAmount,
            int butterAmount,
            int cheeseAmount,
            decimal expectedNetPrice
        )
        {
            // Arrange
            var order = new Order()
            {
                Id = Guid.NewGuid(),
                CreatedDate = DateTime.Now,
                Items = new List<OrderItem<Product>>
                {
                    new OrderItem<Product>(_soup, soupAmount),
                    new OrderItem<Product>(_bread, breadAmount),
                    new OrderItem<Product>(_butter, butterAmount),
                    new OrderItem<Product>(_cheese, cheeseAmount)
                },
                SpecialOffers = new List<SpecialOffer>
				{
                    _buyOneGetOneFree,
                    _buyOneGetOneGetHalfPriceBread,
                    _getThirdOff
				}
            };

            // Act
            await _shopService.ApplySpecialOffers(order);

            // Assert
            order.NetPrice
                 .Should()
                 .Be(expectedNetPrice);
        }

    }
}
