﻿using System;
using Moq;
using System.Collections.Generic;
using GoodEnergy.Services.Shop.Services;
using GoodEnergy.Core.Models;
using GoodEnergy.Core.Handlers;
using GoodEnergy.Core;

namespace GoodEnergy.Services.Shop.Test
{
	public abstract class ShopServiceTest
	{
        protected readonly IShopService _shopService;
        protected readonly IDataHandler<Product> _productDataHandler;
        protected readonly IDataHandler<SpecialOffer> _specialOfferDataHandler;

        protected readonly Product _bread = new Product
        {
            Id = Constants.Product.Bread,
            Name = "Bread",
            Description = "A hearty sprouted wheat bread",
            Price = 2.0m,
            Image = "/images/bread.jpg"
        };

        protected readonly Product _milk = new Product
        {
            Id = Constants.Product.Milk,
            Name = "Milk",
            Description = "The cream of the crop",
            Price = 1.0m,
            Image = "/images/milk.jpg"
        };

        protected readonly Product _cheese = new Product
        {
            Id = Constants.Product.Cheese,
            Name = "Cheese",
            Description = "Not just any chewy calcium lactate crystals",
            Price = 3.0m,
            Image = "/images/cheese.jpg",
            SpecialOffers = new List<SpecialOffer>
            {
                new SpecialOffer
                {
                    Id = Constants.SpecialOffer.BuyOneGetOneFree,
                    Detail = "Buy one get one free!"
                }
            }
        };

        protected readonly Product _soup = new Product
        {
            Id = Constants.Product.Soup,
            Name = "Soup",
            Description = "A spicy carrot and coriander chili chowder",
            Price = 0.50m,
            Image = "/images/soup.jpg",
            SpecialOffers = new List<SpecialOffer>
            {
                new SpecialOffer
                {
                    Id = Constants.SpecialOffer.BuyOneGetHalfPriceBread,
                    Detail = "Buy one get one half price bread!"
                }
            }
        };

        protected readonly Product _butter =  new Product
        {
            Id = Constants.Product.Butter,
            Name = "Butter",
            Description = "It's about great butter",
            Price = 1.50m,
            Image = "/images/butter.jpg",
            SpecialOffers = new List<SpecialOffer>
            {
                new SpecialOffer
                {
                    Id = Constants.SpecialOffer.GetAThirdOff,
                    Detail = "Get a third off!"
                }
            }
        };

        protected readonly SpecialOffer _buyOneGetOneFree = new SpecialOffer
        {
            Id = Constants.SpecialOffer.BuyOneGetOneFree,
            Detail = "Buy one get one free!"
        };

        protected readonly SpecialOffer _buyOneGetOneGetHalfPriceBread = new SpecialOffer
        {
            Id = Constants.SpecialOffer.BuyOneGetHalfPriceBread,
            Detail = "Buy one get one half price bread!"
        };

        protected readonly SpecialOffer _getThirdOff = new SpecialOffer
        {
            Id = Constants.SpecialOffer.GetAThirdOff,
            Detail = "Get a third off!"
        };

        public ShopServiceTest()
        {

            #region Mock Product Data Handler
            var _mockProductDataHandler = new Mock<IDataHandler<Product>>();

            _mockProductDataHandler
                .Setup(x => x.Get(It.Is<Guid>(u => u == _bread.Id)))
                .Returns(_bread);
            _mockProductDataHandler
                .Setup(x => x.Get(It.Is<Guid>(u => u == _milk.Id)))
                .Returns(_milk);
            _mockProductDataHandler
                .Setup(x => x.Get(It.Is<Guid>(u => u == _cheese.Id)))
                .Returns(_cheese);
            _mockProductDataHandler
                .Setup(x => x.Get(It.Is<Guid>(u => u == _soup.Id)))
                .Returns(_soup);
            _mockProductDataHandler
                .Setup(x => x.Get(It.Is<Guid>(u => u == _butter.Id)))
                .Returns(_butter);

            _productDataHandler = _mockProductDataHandler.Object;
            #endregion

            #region Mock Special Offer Data Handler
            var _mockSpecialOfferDataHandler = new Mock<IDataHandler<SpecialOffer>>();

            _mockSpecialOfferDataHandler
                    .Setup(x => x.Get(It.Is<Guid>(u => u == _buyOneGetOneFree.Id)))
                    .Returns(_buyOneGetOneFree);
            _mockSpecialOfferDataHandler
                    .Setup(x => x.Get(It.Is<Guid>(u => u == _buyOneGetOneGetHalfPriceBread.Id)))
                    .Returns(_buyOneGetOneGetHalfPriceBread);
            _mockSpecialOfferDataHandler
                    .Setup(x => x.Get(It.Is<Guid>(u => u == _getThirdOff.Id)))
                    .Returns(_getThirdOff);

            _specialOfferDataHandler = _mockSpecialOfferDataHandler.Object;
            #endregion

            #region Mock Shop Service
            _shopService = new ShopService(
               _productDataHandler,
               _specialOfferDataHandler
            );
            #endregion
        }
    }
}
