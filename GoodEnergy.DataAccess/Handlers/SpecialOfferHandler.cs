﻿using System;
using System.Collections.Generic;
using System.Linq;
using GoodEnergy.Core.Handlers;
using GoodEnergy.Core.Models;

namespace GoodEnergy.DataAccess.Handlers
{
	public class SpecialOfferHandler : IDataHandler<SpecialOffer>
	{
		private readonly DbContext DbContext = DbContext.Instance;

		public SpecialOffer Get(Guid id)
		{
			return DbContext.SpecialOffers.FirstOrDefault(so => so.Id == id);
		}

		public List<SpecialOffer> GetAll()
		{
			return DbContext.SpecialOffers;
		}
	}
}
