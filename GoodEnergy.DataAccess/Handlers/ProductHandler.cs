﻿using System;
using System.Collections.Generic;
using System.Linq;
using GoodEnergy.Core.Handlers;
using GoodEnergy.Core.Models;

namespace GoodEnergy.DataAccess.Handlers
{
	public class ProductHandler : IDataHandler<Product>
	{
		private readonly DbContext DbContext = DbContext.Instance;

		public Product Get(Guid id)
		{
			return DbContext.Products.FirstOrDefault(p => p.Id == id);
		}

		public List<Product> GetAll()
		{
			return DbContext.Products;
		}
	}
}
