﻿using GoodEnergy.Core;
using GoodEnergy.Core.Models;
using System;
using System.Collections.Generic;


namespace GoodEnergy.DataAccess
{
    /// <summary>
	/// NOTE: MOCKED DATABASE
	/// </summary>
    internal sealed class DbContext
    {
        private static readonly DbContext _instance = new DbContext
        {
            Products = GetProductData(),
            SpecialOffers = GetSpecialOfferData(),
        };

        public static DbContext Instance => _instance;

		public List<Product> Products { get; private set; }

		public List<SpecialOffer> SpecialOffers { get; private set; }

		static DbContext() { }

        private DbContext() { }

		#region Data Population
		private static List<Product> GetProductData()
		{
            return new List<Product>
            {
                new Product
                {
                    Id = Constants.Product.Bread,
                    Name = "Bread",
                    Description = "A hearty sprouted wheat bread",
                    Price = 1.10m,
                    Image = "/images/bread.jpg"
                },
                new Product
                {
                    Id = Constants.Product.Milk,
                    Name = "Milk",
                    Description = "The cream of the crop",
                    Price = 0.50m,
                    Image = "/images/milk.jpg"
                },
                new Product
                {
                    Id = Constants.Product.Cheese,
                    Name = "Cheese",
                    Description = "Not just any chewy calcium lactate crystals",
                    Price = 0.90m,
                    Image = "/images/cheese.jpg",
                    SpecialOffers = new List<SpecialOffer>
					{
                        new SpecialOffer
                        {
                            Id = Constants.SpecialOffer.BuyOneGetOneFree,
                            Detail = "Buy one get one free!"
                        }
                    }
                },
                new Product
                {
                    Id = Constants.Product.Soup,
                    Name = "Soup",
                    Description = "A spicy carrot and coriander chili chowder",
                    Price = 0.60m,
                    Image = "/images/soup.jpg",
                    SpecialOffers = new List<SpecialOffer>
                    {
                        new SpecialOffer
                        {
                            Id = Constants.SpecialOffer.BuyOneGetHalfPriceBread,
                            Detail = "Buy one get one half price bread!"
                        }
                    }
                },
                new Product
                {
                    Id = Constants.Product.Butter,
                    Name = "Butter",
                    Description = "It's about great butter",
                    Price = 1.10m,
                    Image = "/images/butter.jpg",
                    SpecialOffers = new List<SpecialOffer>
                    {
                        new SpecialOffer
                        {
                            Id = Constants.SpecialOffer.GetAThirdOff,
                            Detail = "Get a third off!"
                        }
                    }
                }
            };
        }

        private static List<SpecialOffer> GetSpecialOfferData()
		{
            return new List<SpecialOffer>
            {
                new SpecialOffer
                {
                    Id = Constants.SpecialOffer.BuyOneGetOneFree,
                    Detail = "Buy one get one free!"
                },
                new SpecialOffer
                {
                    Id = Constants.SpecialOffer.BuyOneGetHalfPriceBread,
                    Detail = "Buy one get one half price bread!"
                },
                new SpecialOffer
                {
                    Id = Constants.SpecialOffer.GetAThirdOff,
                    Detail = "Get a third off!"
                },
            };
		}
        #endregion
    }
}
