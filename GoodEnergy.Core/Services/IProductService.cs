using GoodEnergy.Core.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GoodEnergy.Core.Services
{
  public interface IProductService
  {
		Task<List<Product>> GetProducts();

		Task<Product> GetProduct(Guid id);
  }
}
