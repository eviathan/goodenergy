﻿using GoodEnergy.Core.Models;
using System;
using System.Threading.Tasks;

namespace GoodEnergy.Core.Services
{
	public interface IOrderService
	{
		Task<Order> CreateOrder(Basket basket);
	}
}
