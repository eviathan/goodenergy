using GoodEnergy.Core.Handlers;
using GoodEnergy.Core.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GoodEnergy.Core.Services
{
  public interface ISpecialOfferService
  {
		Task<List<SpecialOffer>> GetSpecialOffers();

		Task<List<SpecialOffer>> GetSpecialOffersForProduct(Guid productId);

		Task<Order> ApplySpecialOffers(Order order);
	}
}
