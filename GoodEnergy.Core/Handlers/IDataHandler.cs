﻿using GoodEnergy.Core.Models;
using System;
using System.Collections.Generic;

namespace GoodEnergy.Core.Handlers
{
	public interface IDataHandler<TEntity>
		where TEntity : Entity
	{
		TEntity Get(Guid id);

		List<TEntity> GetAll();
	}
}
