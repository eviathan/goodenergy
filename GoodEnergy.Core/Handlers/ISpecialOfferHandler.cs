﻿using GoodEnergy.Core.Models;
using System;

namespace GoodEnergy.Core.Handlers
{
	public interface ISpecialOfferHandler
	{
		Guid SpecialOfferId { get; }

		ISpecialOfferHandler NextHandler { get; set; }

		void ApplyOffer(Order order);
	}
}
