﻿using System;
namespace GoodEnergy.Core
{
	public static class Constants
	{
		public static class Product
		{
			public static readonly Guid Bread = new Guid("00000000-0000-0000-0000-000000000000");
			public static readonly Guid Milk = new Guid("00000000-0000-0000-0000-000000000001");
			public static readonly Guid Cheese = new Guid("00000000-0000-0000-0000-000000000002");
			public static readonly Guid Soup = new Guid("00000000-0000-0000-0000-000000000003");
			public static readonly Guid Butter = new Guid("00000000-0000-0000-0000-000000000004");
		}

		public static class SpecialOffer
		{
			public static readonly Guid BuyOneGetOneFree = new Guid("00000000-0000-0000-0000-000000000000");
			public static readonly Guid BuyOneGetHalfPriceBread = new Guid("00000000-0000-0000-0000-000000000001");
			public static readonly Guid GetAThirdOff = new Guid("00000000-0000-0000-0000-000000000002");
		}
	}
}
