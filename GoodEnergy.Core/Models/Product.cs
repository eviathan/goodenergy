using System;
using System.Collections.Generic;

namespace GoodEnergy.Core.Models
{
    public class Product : Entity
    {
        public string Name { get; set; }

        public string Description { get; set; }

        public decimal Price { get; set; }

		public string Image { get; set; }

        public List<SpecialOffer> SpecialOffers { get; set; } = new List<SpecialOffer>();
	}
}
