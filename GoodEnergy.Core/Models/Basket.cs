﻿using System;
using System.Collections.Generic;

namespace GoodEnergy.Core.Models
{
	public class BasketItem
	{
		public Guid ProductId { get; set; }

		public int Amount { get; set; }
	}

	public class Basket
	{
		public List<BasketItem> Items { get; set; }
	}
}
