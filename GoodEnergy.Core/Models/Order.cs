using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json.Serialization;

namespace GoodEnergy.Core.Models
{
  public class Order : Entity
  {
		[JsonIgnore]
		public DateTime CreatedDate { get; set; } = DateTime.Now;

		public List<OrderItem<Product>> Items { get; set; }

		[JsonIgnore]
		public List<SpecialOffer> SpecialOffers { get; set; }

		public decimal GrossPrice => Items.Sum(i => i.Product.Price * i.Amount);

		public decimal NetPrice => Items.Sum(i => (i.Product.Price * i.Amount) - i.Discount);

		public Order() { }

		public Order(List<OrderItem<Product>> items)
		{
			Id = Guid.NewGuid();
			Items = items;
		}
	}
}

