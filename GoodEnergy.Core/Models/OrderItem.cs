﻿using System;

namespace GoodEnergy.Core.Models
{
	public class OrderItem<TProduct>
		where TProduct : Product
	{
		public TProduct Product { get; set; }

		public int Amount { get; set; }

		public decimal Discount { get; set; }

		public OrderItem(TProduct product, int amount)
		{
			Product = product;
			Amount = amount;
		}
	}
}
