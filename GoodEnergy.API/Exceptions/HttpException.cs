﻿using System;
using System.Net;

namespace GoodEnergy.API.Exceptions
{
	public class HttpException : Exception
	{
		public HttpException(HttpStatusCode status)
		{
			Status = status;
		}

		public HttpStatusCode Status { get; set; } = HttpStatusCode.InternalServerError;

		public object Value { get; set; }
	}
}