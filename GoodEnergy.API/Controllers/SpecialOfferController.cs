﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using GoodEnergy.Core.Models;
using GoodEnergy.API.Exceptions;
using System.Net;
using GoodEnergy.Services.Shop.Services;

namespace GoodEnergy.API.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class SpecialOfferController : ControllerBase
    {
        private readonly ILogger<SpecialOfferController> _logger;
        private readonly IShopService _shopService;

        public SpecialOfferController(ILogger<SpecialOfferController> logger, IShopService shopService)
        {
            _logger = logger;
            _shopService = shopService;
        }

        /// <summary>
        /// Gets all the avaliable Special Offers
        /// </summary>
        [HttpGet]
        public async Task<IEnumerable<SpecialOffer>> Get()
        {
            try
			{
                return await _shopService.GetSpecialOffers();
            }
            catch (Exception ex)
			{
                _logger.LogError(ex.Message, ex);
                throw new HttpException(HttpStatusCode.InternalServerError);
            }
            
        }

        /// <summary>
        /// Gets all Special Offers ascociated with a Product
        /// </summary>
        [HttpGet("{productId}")]
        public async Task<IEnumerable<SpecialOffer>> Get(Guid productId)
        {
            try
			{
                return await _shopService.GetSpecialOffersForProduct(productId);
            }
            catch (Exception ex)
			{
                _logger.LogError(ex.Message, ex);
                throw new HttpException(HttpStatusCode.InternalServerError);
            }
        }
    }
}
