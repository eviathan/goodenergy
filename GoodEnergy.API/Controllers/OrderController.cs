﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using GoodEnergy.Services.Shop.Services;
using GoodEnergy.Core.Models;
using GoodEnergy.API.Exceptions;
using System.Net;
using System;

namespace GoodEnergy.API.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class OrderController : ControllerBase
    {
        private readonly ILogger<OrderController> _logger;
        private readonly IShopService _shopService;

        public OrderController(ILogger<OrderController> logger, IShopService shopService)
        {
            _logger = logger;
            _shopService = shopService;
        }

        /// <summary>
        /// Creates an order and applies any offers to the items in the basket
        /// </summary>
        [HttpPost]
        public async Task<Order> CreateOrder(Basket basket)
        {
            if (!ModelState.IsValid)
                throw new HttpException(HttpStatusCode.BadRequest);

			try
			{
                return await _shopService.CreateOrder(basket);
			}
			catch (Exception ex)
			{
                _logger.LogError(ex.Message, ex);
                throw new HttpException(HttpStatusCode.InternalServerError);
			}
        }
    }
}
