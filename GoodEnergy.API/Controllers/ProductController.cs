﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using GoodEnergy.Core.Models;
using GoodEnergy.API.Exceptions;
using System.Net;
using GoodEnergy.Services.Shop.Services;

namespace GoodEnergy.API.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class ProductController : ControllerBase
    {
        private readonly ILogger<ProductController> _logger;
        private readonly IShopService _shopService;

        public ProductController(ILogger<ProductController> logger, IShopService shopService)
        {
            _logger = logger;
            _shopService = shopService;
        }

        /// <summary>
        /// Gets all the avaliable Products
        /// </summary>
        [HttpGet]
        public async Task<IEnumerable<Product>> Get()
        {
            try
			{
                return await _shopService.GetProducts();
            }
            catch (Exception ex)
			{
                _logger.LogError(ex.Message, ex);
                throw new HttpException(HttpStatusCode.InternalServerError);
            }
            
        }

		/// <summary>
		/// Gets a single Product by Id
		/// </summary>
		[HttpGet("{id}")]
        public async Task<Product> Get(Guid id)
        {
            try
            {
                var product = await _shopService.GetProduct(id);

                if (product == null)
                    throw new HttpException(HttpStatusCode.NotFound);
                else
                    return product;
            }
            catch(HttpException ex)
			{
                _logger.LogError(ex.Message, ex);
                throw;
			}
            catch (Exception ex)
            {
                _logger.LogError(ex.Message, ex);
                throw new HttpException(HttpStatusCode.InternalServerError);
            }

        }
    }
}
