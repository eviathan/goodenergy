﻿using GoodEnergy.API.Exceptions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

public class HttpExceptionFilter : IActionFilter, IOrderedFilter
{
    public int Order { get; } = int.MaxValue - 10;

    public void OnActionExecuting(ActionExecutingContext context) { }

    public void OnActionExecuted(ActionExecutedContext context)
    {
        if (context.Exception is HttpException exception)
        {
            context.Result = new ObjectResult(exception.Value)
            {
                StatusCode = (int)exception.Status,
            };

            context.ExceptionHandled = true;
        }
    }
}