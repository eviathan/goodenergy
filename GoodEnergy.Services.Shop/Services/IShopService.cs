﻿using GoodEnergy.Core.Services;

namespace GoodEnergy.Services.Shop.Services
{
	public interface IShopService : IProductService, IOrderService, ISpecialOfferService { }
}
