﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GoodEnergy.Core.Handlers;
using GoodEnergy.Core.Models;
using GoodEnergy.Core.Services;
using GoodEnergy.Services.Shop.Extensions;

namespace GoodEnergy.Services.Shop.Services
{
    public partial class ShopService : ISpecialOfferService
    {
        public async Task<List<SpecialOffer>> GetSpecialOffers()
		{
            return await Task.FromResult(_specialOfferHandler.GetAll()); 
		}

        public async Task<List<SpecialOffer>> GetSpecialOffersForProduct(Guid productId)
		{
            return await Task.FromResult(_productHandler.Get(productId).SpecialOffers);
		}

        public async Task<Order> ApplySpecialOffers(Order order)
		{
			var offerHandler = await CreateSpecialOfferHandler(order);
			offerHandler?.ApplyOffer(order);			 
			return order;
		}

		private async Task<ISpecialOfferHandler> CreateSpecialOfferHandler(Order order)
		{
			order.SpecialOffers = new List<SpecialOffer>();

			// Get appropriate special offers
			foreach (var item in order.Items)
			{
				var specialOffer = await GetSpecialOffersForProduct(item.Product.Id);

				if (specialOffer != null && specialOffer.Any())
				{
					order.SpecialOffers.AddRange(specialOffer);
				}
			}

			if (!order.SpecialOffers.Any())
				return null;

			// Construct offer handler for relevant special offers
			var firstSpecialOffer = order.SpecialOffers.First();
			ISpecialOfferHandler offerHandler = SpecialOfferHandlers[firstSpecialOffer.Id];
			ISpecialOfferHandler lastOfferHandler = offerHandler;

			foreach (var specialOrder in order.SpecialOffers.Skip(1))
			{
				var nextHandler = SpecialOfferHandlers[specialOrder.Id];
				lastOfferHandler.SetNextHandler(nextHandler);
				lastOfferHandler = nextHandler;
			}

			return offerHandler;
		}
	}
}
