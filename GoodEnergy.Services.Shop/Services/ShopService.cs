﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using GoodEnergy.Core.Handlers;
using GoodEnergy.Core.Models;

namespace GoodEnergy.Services.Shop.Services
{
	public partial class ShopService: IShopService
	{
        private readonly IDataHandler<Product> _productHandler;
        private readonly IDataHandler<SpecialOffer> _specialOfferHandler;

        public Dictionary<Guid, ISpecialOfferHandler> SpecialOfferHandlers { get; private set; }

        public ShopService(IDataHandler<Product> productHandler, IDataHandler<SpecialOffer> specialOfferHandler)
        {
            SpecialOfferHandlers = Assembly.GetExecutingAssembly()
                    .GetTypes()
                    .Where(p => typeof(ISpecialOfferHandler).IsAssignableFrom(p))
                    .Select(t => Activator.CreateInstance(t) as ISpecialOfferHandler)
                    .ToDictionary(x => x.SpecialOfferId, x => x);

            _productHandler = productHandler;
            _specialOfferHandler = specialOfferHandler;
        }
    }
}
