﻿using GoodEnergy.Core.Models;
using GoodEnergy.Core.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GoodEnergy.Services.Shop.Services
{
	public partial class ShopService : IOrderService
	{
		public async Task<Order> CreateOrder(Basket basket)
		{
			var getOrderItemTasks = basket.Items
				.Where(item => item.Amount > 0)
				.Select(async item => {
				var product = await GetProduct(item.ProductId);
				return new OrderItem<Product>(product, item.Amount);
			});

			var orderItems = await Task.WhenAll(getOrderItemTasks);
			var order = new Order(orderItems.ToList());

			order = await ApplySpecialOffers(order);

			return order;
		}
	}
}


