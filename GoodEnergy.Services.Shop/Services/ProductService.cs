﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using GoodEnergy.Core.Models;
using GoodEnergy.Core.Services;

namespace GoodEnergy.Services.Shop.Services
{
    public partial class ShopService : IProductService
    {
        public async Task<List<Product>> GetProducts()
        {
            return await Task.FromResult(_productHandler.GetAll());
        }

        public async Task<Product> GetProduct(Guid id)
        {
            return await Task.FromResult(_productHandler.Get(id));
        }
	}
}
