﻿using GoodEnergy.Core;
using GoodEnergy.Core.Handlers;
using GoodEnergy.Core.Models;
using System;
using System.Linq;

namespace GoodEnergy.Services.Shop.Handlers
{
	public class BuyOneGetOneFreeOfferHandler : ISpecialOfferHandler
	{
		public Guid SpecialOfferId { get; } = Constants.SpecialOffer.BuyOneGetOneFree;

		public ISpecialOfferHandler NextHandler { get; set; }

		public void ApplyOffer(Order order)
		{
			Func<OrderItem<Product>, bool> IsProductWithSpecialOffer =
				item => item.Product.SpecialOffers
								  .Select(offer => offer.Id)
								  .Contains(SpecialOfferId);

			var items = order.Items
							 .Where(i => i != null && i.Amount >= 2)
							 .Where(IsProductWithSpecialOffer);

			foreach (var item in items)
			{
				var freeItems = item.Amount / 2;
				item.Discount = item.Product.Price * freeItems;
			}

			NextHandler?.ApplyOffer(order);
		}
	}
}
