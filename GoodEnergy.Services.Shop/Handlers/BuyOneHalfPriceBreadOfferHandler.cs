﻿using GoodEnergy.Core.Models;
using System;
using GoodEnergy.Core.Handlers;
using System.Linq;
using GoodEnergy.Core;
using System.Collections;
using System.Collections.Generic;

namespace GoodEnergy.Services.Shop.Handlers
{
	public class BuyOneHalfPriceBreadOfferHandler : ISpecialOfferHandler
	{
		public Guid SpecialOfferId { get; } = Constants.SpecialOffer.BuyOneGetHalfPriceBread;

		public ISpecialOfferHandler NextHandler { get;set; }

		public void ApplyOffer(Order order)
		{
			Func<OrderItem<Product>, bool> IsProductWithSpecialOffer =
				item => item.Product.SpecialOffers
								  .Select(offer => offer.Id)
								  .Contains(SpecialOfferId);

			var items = order.Items
							 .Where(item => item != null && IsProductWithSpecialOffer(item));

			var bread = order.Items.FirstOrDefault(i => i.Product.Id == Constants.Product.Bread);

			if (bread != null)
			{
				var discountBreadStack = new Stack<int>(Enumerable.Range(0, bread.Amount));

				foreach (var item in items)
				{
					if (discountBreadStack.TryPop(out var b))
					{
						item.Discount = (bread.Product.Price * 0.5m) * item.Amount;
					}
					else
					{
						break;
					}
				}
			}

			NextHandler?.ApplyOffer(order);
		}
	}
}
