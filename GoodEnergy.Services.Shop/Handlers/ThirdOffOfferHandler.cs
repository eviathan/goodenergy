﻿using GoodEnergy.Core;
using GoodEnergy.Core.Handlers;
using GoodEnergy.Core.Models;
using System;
using System.Linq;

namespace GoodEnergy.Services.Shop.Handlers
{
	public class ThirdOffOfferHandler : ISpecialOfferHandler
	{
		public Guid SpecialOfferId { get; } = Constants.SpecialOffer.GetAThirdOff;

		public ISpecialOfferHandler NextHandler { get; set; }

		public void ApplyOffer(Order order)
		{
			Func<OrderItem<Product>, bool> IsProductWithSpecialOffer =
				item => item.Product.SpecialOffers
								  .Select(offer => offer.Id)
								  .Contains(SpecialOfferId);

			var items = order.Items
								.Where(i => i != null)
								.Where(IsProductWithSpecialOffer);

			foreach (var item in items)
			{
				item.Discount = item.Product.Price * item.Amount / 3m;
			}

			NextHandler?.ApplyOffer(order);
		}
	}
}
