﻿using GoodEnergy.Core.Handlers;

namespace GoodEnergy.Services.Shop.Extensions
{
	public static class SpecialOfferExtensions
	{
		public static ISpecialOfferHandler SetNextHandler(this ISpecialOfferHandler handler, ISpecialOfferHandler nextHandler)
		{
			handler.NextHandler = nextHandler;
			return handler;
		}
	}
}
