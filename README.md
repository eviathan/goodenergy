# Good Energy | Full Stack Developer - Practical Exercise
## Dependencies
- .NET 5
- Node 14.9

## Build & Run

1. Build Sollution

2. Run GoodEnergy.UI Project
    - Url: https://localhost:8081/

3. Run GoodEnergy.Api Project
    - Api Docs: https://localhost:5001/swagger/


## Brief
 Full Stack Developer – Practical Exercise
Task
You’ve been tasked with writing an application that can receive a selection of the below products and calculate the bill. The bill should include:

- Subtotal before special offers applied
- Special offers applied and their individual saving
- Final total with savings applied

The implementation should be one of either:

- A C# console application
- A React application
- An Android application

The implementation should be supported by suitable unit tests and should demonstrate not only the complete functionality but also represent coding best practices. If a UI is provided, we'll be looking at it - so feel free to make it look cool!

When you've completed the code challenge please let us know and we'll send an invitation to a shared folder to drop your exercise into.


### Scenarios
- Buy a soup and two breads - only one bread should be reduced
- Buy three cheeses - only one should be free
- Buy four cheeses - two now should be free
- Butter alone
- Butter with other things
- A mixture of the above scenarios

### Products & prices
| Product | Price |
|---------|-------|
| Bread   | £1.10 |
| Milk    | £0.50 |
| Cheese  | £0.90 |
| Soup    | £0.06 |
| Butter  | £1.20 |
           

### Special offers
| Offer Details                                        |
|------------------------------------------------------|
| When you buy a Cheese, you get a second Cheese free! |
| When you buy a Soup, you get a half price Bread!     |
| Get a third off Butter!                              |
